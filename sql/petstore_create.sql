-- create category table
create table category (
   id INT NOT NULL auto_increment,
   name VARCHAR(64) default NULL,
   PRIMARY KEY (id)
);

-- create pet table
create table pet (
   id INT NOT NULL auto_increment,
   name VARCHAR(64) NOT NULL,
   category_id INT NOT NULL,
   status VARCHAR(24) NOT NULL,
   photo_url VARCHAR(256) NULL,
   tag VARCHAR(32) NULL,
   description TEXT NULL,
   birth_date DATE NULL,
   price DECIMAL(10,2) NOT NULL,
   PRIMARY KEY (id),
   CONSTRAINT `FK_CATEGORY_ID` FOREIGN KEY (`category_id`)
             REFERENCES `category` (`id`)
);
