// Pet store service interface
angular.module('petStore').service('storeSvc', ['$http', function($http){

	// get all pets from service
	this.getPets = function() {

		return $http.get("pet");
	}

	// get specific pet from service
	this.getPetById = function(petId) {

		return $http.get("pet/" + petId);
	}

	// save new pet with service
	this.createPet = function(pet) {

		return $http({
			method : "POST",
			url:  "pet",
			headers: {
				"Content-Type": "application/json"
			},
			data: pet
		});
	}

	// delete specific pet with service
	this.deletePetById = function(petId) {

		return $http.delete("pet/" + petId);
	}
	
	// get all categories from service
	this.getCategories = function() {

		return $http.get("category");
	}

	// authenticate current user
	this.authenticate = function() {

		return $http.get("authenticate");
	}

	// get principal for authenticated user
	this.getPrincipal = function() {

		return $http.get("principal");
	}

}]);