// application configuration

var app = angular.module("petStore", ["ngRoute", "ngAnimate", "ngSanitize", "ui.bootstrap"]);

app.config(function($routeProvider) {
    
    $routeProvider
    .when("/", {
        templateUrl : "views/store.html",
        controller: "storeCtrl"
    })
    .when("/petStore", {
        templateUrl : "views/store.html",
        controller: "storeCtrl"
    })
    .when("/login", {
        templateUrl : "views/store.html",
        controller: "storeCtrl"
    });
});
