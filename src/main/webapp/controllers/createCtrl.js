// Pet creation controller
angular.module('petStore').controller('createCtrl', ['$scope', '$uibModal', '$uibModalInstance', 'storeSvc', 'storeScope', function($scope, $uibModal, $uibModalInstance, storeSvc, storeScope) {

	// scope variables
	$scope.categories = null;
	$scope.name = null;
	$scope.selectedCategory = null;
	$scope.price = null;
	$scope.description = null;
	$scope.birthDate = null;
	$scope.tagNumber = null;
	$scope.photoUrl = null;
	$scope.validationError = null;


	// retrieve all categories
	storeSvc.getCategories()
		.success(function(data) {
			
			$scope.categories = data;
		})
		.error(function(error) {
			
			console.log("Error retrieving categories from service");
			console.log(error);
		});


	// save newly created pet
	$scope.savePet = function() {
		
		// validate form input
		validateInput();
		if ($scope.validationError) {

			$("#createModalBody").animate({ scrollTop: 0 }, "fast");
			return;
		}

		// create pet object for service submission
		var pet = {};
		pet.name = $scope.name;
		pet.category = $scope.selectedCategory;
		pet.price = $scope.price;
		pet.description = $scope.description;
		pet.birthDate = $scope.birthDate;
		pet.photoUrls = [];
		if ($scope.photoUrl) {

			pet.photoUrls.push($scope.photoUrl);
		}
		pet.tags = [];
		if ($scope.tagNumber) {

			var tag = {};
			tag.id = 0;
			tag.name = $scope.tagNumber;
			pet.tags.push(tag);
		}
		pet.status = "available";

		// submit pet to service
		storeSvc.createPet(pet)
			.success(function(data) {
				
				// add newly created pet to store
				storeScope.pets.push(data);
				$scope.closeModal();				
			})
			.error(function(error) {
				
				console.log("Error creating pet with service");
				console.log(error);
			});	
	}


	// close current modal
	$scope.closeModal = function() {

		$uibModalInstance.dismiss('close');
	}


	// validate user input
	function validateInput() {

		$scope.validationError = null;
		if (!$scope.name)
		{
			$scope.validationError = "Field Name is required.";
		}
		else if (!$scope.selectedCategory)
		{
			$scope.validationError = "Field Category is required.";
		}
		else if ($scope.price === null)
		{
			$scope.validationError = "Field Price is required.";
		}
		else if (getDecimalPlaces($scope.price) > 2)
		{
			$scope.validationError = "Field Price is not valid.";
		}
	}


	// get number of decimal places from number
	// Source: http://stackoverflow.com/questions/10454518/javascript-how-to-retrieve-the-number-of-decimals-of-a-string-number
	function getDecimalPlaces(num) {
		var match = (''+num).match(/(?:\.(\d+))?(?:[eE]([+-]?\d+))?$/);
		if (!match) { return 0; }
  		return Math.max(
       		0,
       		// Number of digits right of decimal point.
       		(match[1] ? match[1].length : 0)
       		// Adjust for scientific notation.
       		- (match[2] ? +match[2] : 0));
	}

}]);