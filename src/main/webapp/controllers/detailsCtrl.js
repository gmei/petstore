// Pet details controller
angular.module('petStore').controller('detailsCtrl', ['$scope', '$uibModal', '$uibModalInstance', 'storeSvc', 'petId', function($scope, $uibModal, $uibModalInstance, storeSvc, petId) {

	$scope.pet = null;

	// retrieve specific pet
	storeSvc.getPetById(petId)
		.success(function(data) {
			
			$scope.pet = data;
		})
		.error(function(error) {
			
			console.log("Error retrieving pet from service");
			console.log(error);
		});


	// close current modal
	$scope.closeModal = function() {

		$uibModalInstance.dismiss('close');
	}

}]);