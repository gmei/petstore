// Navigation bar controller
angular.module('petStore').controller('navigationCtrl', ['$scope', '$rootScope', 'storeSvc', function($scope, $rootScope, storeSvc) {

	// security roles for authenticated user
	$rootScope.isAuthenticated = false;
	$rootScope.isAdministrator = false;


	// check if user is already authenticated
	storeSvc.getPrincipal()
		.success(function(data) {
			
			// process response
			checkPrincipal(data);
		})
		.error(function(error) {
			
			console.log("Failed to get principal from service");
			console.log(error);
		});


	// authenticate the current user
	$scope.login = function()
	{
		storeSvc.authenticate()
			.success(function(data) {
				
				// process response
				checkPrincipal(data);
			})
			.error(function(error) {
				
				console.log("Failed to authenticate user");
				console.log(error);
			});
	}


	// process security principal returned from service
	function checkPrincipal(response) {

		// user is authenticated if response not null
		if (response)
		{
			$rootScope.isAuthenticated = true;
			console.log("User successfully authenticated");
			
			// check if user is administrator
			if (response.principal.authorities)
			{
				for (var i = 0; i < response.principal.authorities.length; i++)
				{
					if (response.principal.authorities[i].authority)
					{
						if (response.principal.authorities[i].authority == "ROLE_ADMIN")
						{
							$rootScope.isAdministrator = true;
							console.log("User is administrator");
							break;
						}
					}
				}
			}
		}
	}

}]);