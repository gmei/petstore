// Pet deletion controller
angular.module('petStore').controller('deleteCtrl', ['$scope', '$uibModal', '$uibModalInstance', 'storeSvc', 'storeScope', 'pet', function($scope, $uibModal, $uibModalInstance, storeSvc, storeScope, pet) {

	$scope.pet = pet;

	// save newly created pet
	$scope.deletePet = function() {
		
		// delete pet with service
		storeSvc.deletePetById(pet.id)
			.success(function(data) {
				
				// remove deleted pet from store's model
				for (var i = 0; i < storeScope.pets.length; i++) {
					
					if (storeScope.pets[i].id == pet.id) {

						storeScope.pets.splice(i, 1);
						break;
					}
				}

				$scope.closeModal();				
			})
			.error(function(error) {
				
				console.log("Error deleting pet with service");
				console.log(error);
			});	
	}


	// close current modal
	$scope.closeModal = function() {

		$uibModalInstance.dismiss('close');
	}

}]);