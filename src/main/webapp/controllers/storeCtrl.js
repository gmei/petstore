// Main pet store controller
angular.module('petStore').controller('storeCtrl', ['$scope', '$uibModal', 'storeSvc', function($scope, $uibModal, storeSvc) {

	// scope variables
	$scope.pets = null;

	// local variables
	var modalInstance = null;

	// retrieve all pets
	storeSvc.getPets()
		.success(function(data) {
			
			$scope.pets = data;
		})
		.error(function(error) {
			
			console.log("Error retrieving pets from service");
			console.log(error);
		});


	// open modal to view pet details
	$scope.viewPet = function(pet) {
		
		$uibModal.open({
			templateUrl: "views/detailModal.html",
			controller: "detailsCtrl",
			resolve: {
				petId : function () {
					return pet.id;
				}
			}
    	});
	};


	// open modal to create new pet
	$scope.createPet = function() {
		
		$uibModal.open({
			templateUrl: "views/createModal.html",
			controller: "createCtrl",
			resolve: {
				storeScope : function () {
					return $scope;
				}
			}
    	});
	};


	// open modal to delete pet
	$scope.deletePet = function(pet) {
		
		$uibModal.open({
			templateUrl: "views/deleteModal.html",
			controller: "deleteCtrl",
			resolve: {
				storeScope : function () {
					return $scope;
				},
				pet : function () {
					return pet;
				}
			}
    	});
	};

}]);