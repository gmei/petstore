package com.petstore.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

@Component
public interface CategoryRepository extends CrudRepository<CategoryEntity, Integer> {

}
