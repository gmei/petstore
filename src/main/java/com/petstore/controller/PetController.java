package com.petstore.controller;

import java.util.List;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;

import com.google.gson.Gson;
import com.petstore.dto.Pet;
import com.petstore.service.PetService;

/**
 * Pet controller, end-point for pet REST service.
 * 
 * @author grant
 */
@RestController
public class PetController {

	// logger for class
	private final Logger logger = LoggerFactory.getLogger(this.getClass());
	
	// gson instance for JSON conversion
	private static Gson gson = new Gson();
	
	@Autowired
	PetService petService;
	
	/**
	 * Retrieves list of pets from data store.
	 * 
	 * @return List<Pet> List of pets
	 */
	@RequestMapping(value="/pet", method=RequestMethod.GET)
    public List<Pet> getAllPets() 
    {
		logger.info("getAllPets start");
		List<Pet> list = null;
		
		try
		{
			list = petService.findAllPets();
		}
		catch (Exception e)
		{
			logger.error("getAllPets error: " + e.getMessage());
			logger.error(e.getStackTrace().toString());
			throw e;
		}
		
		logger.debug(gson.toJson(list));
		logger.info("getAllPets end");
		
        return list;
    }
	
	
	/**
	 * Creates a new pet and adds it to the data store.
	 * 
	 * @param Pet new pet to be created
	 * @return Pet newly created Pet instance (including generated id)
	 */
	@RequestMapping(value="/pet", method=RequestMethod.POST)
    public Pet createPet(@RequestBody Pet pet) 
    {
		logger.info("createPet start");
		logger.debug(gson.toJson(pet));
		Pet createdPet = null;
				
		try
		{
			createdPet = petService.createPet(pet);
		}
		catch (Exception e)
		{
			logger.error("createPet error: " + e.getMessage());
			logger.error(e.getStackTrace().toString());
			throw e;
		}
		
		logger.debug(gson.toJson(createdPet));
		logger.info("createPet end");
		
		return createdPet;   
    }
	
	
	/**
	 * Retrieves pet with a specified id from data store.
	 * 
	 * @param petId Pet Id for query
	 * @return Pet with the provided id
	 */
	@RequestMapping(value="/pet/{petId}", method=RequestMethod.GET)
    public Pet getPetById(@PathVariable(value="petId") int petId) 
    {
		logger.info("getPetById start for id: " + petId);
		Pet pet = null;
        
		try
		{
			pet = petService.getPetById(petId);
		}
		catch (Exception e)
		{
			logger.error("getPetById error: " + e.getMessage());
			logger.error(e.getStackTrace().toString());
			throw e;
		}
		
		logger.debug(gson.toJson(pet));
		logger.info("getPetById end");
		
		return pet;    
    }
	
	
	/**
	 * Removes a pet from the data store
	 * 
	 * @param petId int id of the pet to be deleted
	 */
	@RequestMapping(value="/pet/{petId}", method=RequestMethod.DELETE)
    public void deletePetById(@PathVariable(value="petId") int petId) 
    {
		logger.info("deletePetById start for id: " + petId);
		
		try
		{
			petService.deletePetById(petId);
		}
		catch (Exception e)
		{
			logger.error("deletePetById error: " + e.getMessage());
			logger.error(e.getStackTrace().toString());
			throw e;
		}
        
		logger.info("deletePetById end");
    }
}
