package com.petstore.controller;

import java.security.Principal;
import java.util.List;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.beans.factory.annotation.Autowired;


/**
 * Security controller, end-point for authentication
 * 
 * @author grant
 */
@RestController
public class SecurityController {
	
	/**
	 * Authenticate user
	 * 
	 * @return Principal for authenticated user
	 */
	@RequestMapping(value="/authenticate", method=RequestMethod.GET)
    public Principal authenticate(Principal user) 
    {
		return user;
    }
	
	
	/**
	 * Retrieve principal for authenticated user
	 * 
	 * @return Principal for authenticated user
	 */
	@RequestMapping(value="/principal", method=RequestMethod.GET)
    public Principal principal(Principal user) 
    {
		return user;
    }
}
