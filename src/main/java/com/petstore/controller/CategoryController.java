package com.petstore.controller;

import java.util.List;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.beans.factory.annotation.Autowired;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.Gson;
import com.petstore.dto.Category;
import com.petstore.service.CategoryService;

/**
 * Category controller, end-point for category REST service.
 * 
 * @author grant
 */
@RestController
public class CategoryController {
	
	// logger for class
	private final Logger logger = LoggerFactory.getLogger(this.getClass());
	
	// gson instance for JSON conversion
	private static Gson gson = new Gson();	
	
	@Autowired
	CategoryService categoryService;
	
	/**
	 * Retrieves list of pet categories from data store.
	 * 
	 * @return List<Category> List of categories
	 */
	@RequestMapping(value="/category", method=RequestMethod.GET)
    public List<Category> getAllCategories() 
    {
		logger.info("getAllCategories start");
		List<Category> list = null;
		
		try
		{
			list = categoryService.findAllCategories();
		}
		catch (Exception e)
		{
			logger.error("getAllCategories error: " + e.getMessage());
			logger.error(e.getStackTrace().toString());
			throw e;
		}
		
		logger.debug(gson.toJson(list));
		logger.info("getAllCategories end");
		
        return list;
    }
	
}
