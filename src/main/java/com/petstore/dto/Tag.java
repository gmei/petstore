package com.petstore.dto;

/**
 * Container for pet tag
 * 
 * @author grant
 */
public class Tag {

	private int id;
	private String name;
	
	public Tag()
	{
		// nothing here..
	}
	
	public Tag(int id, String name)
	{
		this.id = id;
		this.name = name;
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
}
