package com.petstore.service;

import java.util.ArrayList;
import java.util.List;

import com.petstore.dto.Category;
import com.petstore.dto.Pet;
import com.petstore.dto.Tag;
import com.petstore.repository.CategoryEntity;
import com.petstore.repository.PetEntity;

/**
 * DataMapper, maps entity objects to dto and vice-versa
 * 
 * @author grant
 */
public class DataMapper {

	public static Category categoryEntityToDto(CategoryEntity categoryEntity)
	{
		Category category = null;
		
		if (categoryEntity != null)
		{
			category = new Category();
			category.setId(categoryEntity.getId());
			category.setName(categoryEntity.getName());
		}
		
		return category;
	}
	
	public static Pet petEntityToDto(PetEntity petEntity)
	{
		Pet pet = null;
		
		if (petEntity != null)
		{
			pet = new Pet();
			pet.setId(petEntity.getId());
			pet.setName(petEntity.getName());
			pet.setCategory(categoryEntityToDto(petEntity.getCategory()));
			pet.setStatus(petEntity.getStatus());
			
			// convert photo url to list
			List<String> photoUrls = new ArrayList<String>();
			if (petEntity.getPhotoUrl() != null && !petEntity.getPhotoUrl().isEmpty())
			{
				photoUrls.add(petEntity.getPhotoUrl());
			}
			pet.setPhotoUrls(photoUrls);
			
			// convert tag to list
			List<Tag> tags = new ArrayList<Tag>();
			if (petEntity.getTag() != null && !petEntity.getTag().isEmpty())
			{
				tags.add(new Tag(0, petEntity.getTag()));
			}
			pet.setTags(tags);
			
			pet.setDescription(petEntity.getDescription());
			pet.setBirthDate(petEntity.getBirthDate());
			pet.setPrice(petEntity.getPrice());
		}
		
		return pet;
	}
	
	public static CategoryEntity categoryDtoToEntity(Category category)
	{
		CategoryEntity categoryEntity = null;
		
		if (category != null)
		{
			categoryEntity = new CategoryEntity();
			categoryEntity.setId(category.getId());
			categoryEntity.setName(category.getName());
		}
		
		return categoryEntity;
	}
	
	public static PetEntity petDtoToEntity(Pet pet)
	{
		PetEntity petEntity = null;
		
		if (pet != null)
		{
			petEntity = new PetEntity();
			petEntity.setId(pet.getId());
			petEntity.setName(pet.getName());
			petEntity.setCategory(categoryDtoToEntity(pet.getCategory()));
			petEntity.setStatus(pet.getStatus());
			
			// if photo url list has entry, use first element
			if (pet.getPhotoUrls() != null && pet.getPhotoUrls().size() > 0)
			{
				petEntity.setPhotoUrl(pet.getPhotoUrls().get(0));
			}
			
			// if tag list has entry, use first element
			if (pet.getTags() != null && pet.getTags().size() > 0)
			{
				petEntity.setTag(pet.getTags().get(0).getName());
			}
			
			petEntity.setDescription(pet.getDescription());
			petEntity.setBirthDate(pet.getBirthDate());
			petEntity.setPrice(pet.getPrice());
		}
		
		return petEntity;
	}
	
}
