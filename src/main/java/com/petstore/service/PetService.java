package com.petstore.service;

import java.util.List;

import com.petstore.dto.Pet;

public interface PetService {

	List<Pet> findAllPets();
	
	Pet createPet(Pet pet);
	
	Pet getPetById(Integer petId);
	
	void deletePetById(Integer petId);
}
