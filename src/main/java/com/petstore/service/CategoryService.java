package com.petstore.service;

import java.util.List;

import com.petstore.dto.Category;

public interface CategoryService {

	List<Category> findAllCategories();
	
}
