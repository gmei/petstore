package com.petstore.service;

import java.util.ArrayList;
import java.util.List;

import org.apache.catalina.mapper.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.petstore.dto.Category;
import com.petstore.dto.Pet;
import com.petstore.repository.CategoryEntity;
import com.petstore.repository.CategoryRepository;
import com.petstore.repository.PetEntity;
import com.petstore.repository.PetRepository;

@Service
@Transactional
public class PetServiceImpl implements PetService {

	@Autowired
	PetRepository petRepository;
	
	public List<Pet> findAllPets()
	{
        List<PetEntity> entityList = (List<PetEntity>) petRepository.findAll();
        
        // convert data access objects to dto
        List<Pet> petList = new ArrayList<Pet>();
        for (PetEntity petEntity : entityList)
        {
        	petList.add(DataMapper.petEntityToDto(petEntity));
        }
        
        return petList;
	}
	
	public Pet createPet(Pet pet)
	{
		PetEntity petEntity = DataMapper.petDtoToEntity(pet);
		PetEntity savedEntity = petRepository.save(petEntity);
		
		return DataMapper.petEntityToDto(savedEntity);
	}
	
	public Pet getPetById(Integer petId)
	{
		PetEntity petEntity = petRepository.findOne(petId);
		
		return DataMapper.petEntityToDto(petEntity);	
	}
	
	public void deletePetById(Integer petId)
	{
		petRepository.delete(petId);
	}
	
}


