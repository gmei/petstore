package com.petstore.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.petstore.dto.Category;
import com.petstore.repository.CategoryEntity;
import com.petstore.repository.CategoryRepository;

@Service
@Transactional
public class CategoryServiceImpl implements CategoryService {

	@Autowired
	CategoryRepository categoryRepository;
	
	public List<Category> findAllCategories()
	{
        List<CategoryEntity> entityList = (List<CategoryEntity>) categoryRepository.findAll();
        
        // convert data access objects to dto
        List<Category> categoryList = new ArrayList<Category>();
        for (CategoryEntity categoryEntity : entityList)
        {
        	categoryList.add(DataMapper.categoryEntityToDto(categoryEntity));
        }
        
        return categoryList;
	}
	

}


