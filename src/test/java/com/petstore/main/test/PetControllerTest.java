package com.petstore.main.test;

import static org.junit.Assert.assertEquals;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import com.petstore.controller.CategoryController;
import com.petstore.controller.PetController;
import com.petstore.dto.Category;
import com.petstore.dto.Pet;
import com.petstore.dto.Tag;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
public class PetControllerTest {
	
	@Autowired
	private PetController petController;
	
	@Autowired 
	private CategoryController categoryController;
	
	@Test
	public void TestPetController()
	{
		// get original pet list
		List<Pet> petList = petController.getAllPets();
		int origSize = petList.size();
		
		// get category list
		List<Category> categoryList = categoryController.getAllCategories();
		
		// create new pet
		Pet newPet = new Pet();
		newPet.setName("Test Pet");
		newPet.setCategory(categoryList.get(0));
		newPet.setStatus("available");
		newPet.setPhotoUrls(new ArrayList<String>(Arrays.asList("http://grant.org")));
		newPet.setTags(new ArrayList<Tag>(Arrays.asList(new Tag(0, "1234567890"))));
		newPet.setDescription("This is a test pet");
		newPet.setBirthDate(new Date());
		newPet.setPrice(123.45);
		
		// add new pet to data store
		newPet = petController.createPet(newPet);
		
		// get updated pet list
		List<Pet> updatedPetList = petController.getAllPets();
		int updatedSize = updatedPetList.size();
		
		// check if pet is added
		assertEquals(origSize + 1, updatedSize);
		
		// remove newly created pet
		petController.deletePetById(newPet.getId());
		
		// get updated pet list
		updatedPetList = petController.getAllPets();
		updatedSize = updatedPetList.size();
		
		// check if pet is removed
		assertEquals(origSize, updatedSize);
	}
}
