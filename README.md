# Pet Store #

This is a sample web application for a pet store.

### Functionality ###

* Anonymous users permitted to view pets available for sale.
* Logged in registered users permitted to create new pets for sale.
* Logged in administrator users permitted to delete existing pets.

### Technical Summary ###

* Front end: AngularJS, Bootstrap
* Back end: Java REST services utilizing Spring Boot, Spring Security, Spring Data (JPA)
    * REST service specification: http://petstore.swagger.io/#/
* Database: MySQL

### Source Code ###

* src/main/java: Java source code
* src/main/resources: Application properties
* src/main/webapp: AngularJS web application
* src/test/java: JUnit test cases
* sql: MySQL database scripts for table creation and population